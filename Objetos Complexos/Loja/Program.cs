﻿using Loja.Dominio.Entidades;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Loja {
	class Program {
		static void Main(string[] args) {
			var produto = new Produto();

			#region Linq e Lambda
			//var resultado = from prod in produto.Listar()
			//								where prod.Valor > 6
			//								orderby prod.Nome
			//								select prod;

			//var produtos = produto.Listar().Where(p => p.Valor > 6).OrderBy(p => p.Nome);

			//Console.WriteLine("== Lista de Produtos ==");
			//foreach (Produto item in produtos) {
			//	Console.WriteLine("Id: {0}", item.Id);
			//	Console.WriteLine("Nome: {0}", item.Nome);
			//	Console.WriteLine("Quantidade: {0}", item.Quantidade);
			//	Console.WriteLine("Valor: {0}", item.Valor);
			//	Console.WriteLine("Data de Vencimento: {0}", item.DataVencimento.ToString("dd/MM/yyyy"));
			//	Console.WriteLine("============================================");

			//Console.WriteLine("== Lista de Produtos ==\n"); // Where retorna IEnumerable e ForEach trabalha em cima de lista...
			//var produtos = new Produto().Listar();

			//produtos = produtos.Where(p => p.Nome.StartsWith("A") || 
			//													p.Nome.StartsWith("C") || 
			//													p.Nome.EndsWith("a"))
			//													.OrderBy(p => p.Nome)
			//													.ToList();

			//produtos.ForEach(item => {
			//	Console.WriteLine();
			//	Console.WriteLine(JsonConvert.SerializeObject(item));
			//});

			//Console.WriteLine("== Lista de Produtos ==\n"); // Where retorna IEnumerable e ForEach trabalha em cima de lista...
			//var produtos = new Produto().Listar();

			//var nomes = produtos
			//	//.Where(p => p.Nome.StartsWith("A") || p.Nome.StartsWith("C") ||	p.Nome.EndsWith("a"))
			//	//.Where(p => p.DataValidade.Year == DateTime.Now.AddYears(1).Year)
			//	.OrderBy(p => p.Nome)
			//	.Select(p => new ProdutosSelecionado {
			//		Nome = p.Nome,
			//		Valor = p.Valor,
			//		DiaValidade = p.DataValidade.Day,
			//		MesValidade = p.DataValidade.Month,
			//		AnoValidade = p.DataValidade.Year
			//	}
			//	)
			//	.ToList();

			//nomes.ForEach(item => {
			//	Console.WriteLine();
			//	Console.WriteLine(JsonConvert.SerializeObject(item));
			//});

			//Console.WriteLine("== Lista de Produtos ==\n"); // Where retorna IEnumerable e ForEach trabalha em cima de lista...
			//var produtos = new Produto().Listar();

			////if (produtos.Any(p => p.Quantidade > 10)) {
			//	Console.WriteLine("Existem produtos");
			//}
			//var produto1 = produtos.First();
			//var produto2 = produtos.FirstOrDefault();
			//var produto1 = produtos.Last();
			//var produto2 = produtos.LastOrDefault();

			//Console.WriteLine();
			//Console.WriteLine(JsonConvert.SerializeObject(produto1));
			//Console.WriteLine();
			//Console.WriteLine(JsonConvert.SerializeObject(produto2));

			//int[] numerosPares = { 2, 4, 6, 8, 10 };
			//int[] numerosImpares = { 1, 3, 5, 7, 9 };
			//int[] numerosMisturados = { 1, 2, 3, 4 };

			//Console.WriteLine("== Números Pares");
			//foreach (var numero in numerosPares) {
			//	Console.Write(numero + " ");
			//}

			//Console.WriteLine();
			//Console.WriteLine("== Números Ímpares");
			//foreach (var numero in numerosImpares) {
			//	Console.Write(numero + " ");
			//}

			//Console.WriteLine();
			//Console.WriteLine("== Números misturados");
			//foreach (var numero in numerosPares) {
			//	Console.Write(numero + " ");
			//}

			//var numerosComuns = numerosMisturados.Intersect(numerosPares);
			//var numerosIncomuns = numerosMisturados.Except(numerosPares);

			//Console.WriteLine();
			//Console.WriteLine("== Números comuns");
			//foreach(var numero in numerosComuns) {
			//	Console.Write(numero + " ");
			//}

			//Console.WriteLine();
			//Console.WriteLine("== Números incomuns");
			//foreach (var numero in numerosIncomuns) {
			//	Console.Write(numero + " ");
			//}

			//var sequencia = Enumerable.Range(1, 150);

			//Console.WriteLine("== Sequência");
			//foreach (var numero in sequencia) {
			//	Console.Write(numero + " ");
			//}
			//Console.WriteLine();

			//var repetir= Enumerable.Repeat('*', 45);

			//Console.WriteLine("== Repetir");
			//foreach (var item in repetir) {
			//	Console.Write(item);
			//}
			//Console.WriteLine();

			//var produtos = new Produto().Listar();
			//// Maior valor
			//var maior = produtos.Max(p => p.Valor);
			//// Menor valor
			//var menor = produtos.Min(p => p.Valor);
			////Média dos valores
			//var media = produtos.Average(p => p.Valor);
			////Soma dos valores
			//var soma = produtos.Sum(p => p.Valor); 
			#endregion

			#region Agrupamento e Agregação
			//var produtos = new Produto().Listar().OrderBy(p=>(p.Categoria, p.Nome)).ToList();

			//Console.WriteLine("== Lista de produtos serializada");
			//Console.WriteLine();
			//produtos.ForEach(p => {
			//	Console.WriteLine(JsonConvert.SerializeObject(p));
			//	Console.WriteLine(String.Concat(Enumerable.Repeat('=', 60)));
			//});

			//var resultadoAgrupamento = (from p in produtos
			//													 group p by p.Categoria into grupo
			//													 select new ProdutoPorCategoria {
			//														 Categora = grupo.Key,
			//														 ValorMinimo = grupo.Min(x => x.Valor),
			//														 ValorMaximo = grupo.Max(x => x.Valor),
			//														 ValorTotal = grupo.Sum(x=>x.Valor)
			//													 }).OrderBy(x=>x.Categora);

			//Console.WriteLine();
			//Console.WriteLine("== Lista de produtos serializada [Agrupada por Categoria]");
			//Console.WriteLine();
			//resultadoAgrupamento.ToList().ForEach(p => {
			//	Console.WriteLine(JsonConvert.SerializeObject(p));
			//	Console.WriteLine(String.Concat(Enumerable.Repeat('=', 60)));
			//}); 
			#endregion

			// Paralelismo
			try {
				int qtde = 10000;
				string tempoProcNormal = ProcessamentoNormal(qtde);
				string tempoProcParalelo = ProcessamentoParalelo(qtde);
				Console.WriteLine();
				Console.WriteLine("Tempo de processamento normal {0}", tempoProcNormal);
				Console.WriteLine("Tempo de processamento paralelo {0}", tempoProcParalelo);

				Console.ReadKey();
			} catch (Exception e) {
				Console.WriteLine("Exceção: {0}", e.Message);
			}
		}

		private static string ProcessamentoNormal(int qtde) {
			Stopwatch sw = new Stopwatch();
			sw.Start();

			for (int i = 0; i < qtde; i++) {
				Console.Write("{0}, ", i.ToString());
			}

			sw.Stop();
			return sw.Elapsed.ToString();
		}

		private static string ProcessamentoParalelo(int qtde) {
			Stopwatch sw = new Stopwatch();
			sw.Start();

			Parallel.For(0, qtde, index => {
				Console.Write("{0}, ", index.ToString());
			});

			sw.Stop();
			return sw.Elapsed.ToString();
		}
	}

	public class ProdutosSelecionado {
		public string Nome { get; set; }
		public decimal Valor { get; set; }
		public int DiaValidade { get; set; }
		public int MesValidade { get; set; }
		public int AnoValidade { get; set; }
	}

	public class ProdutoPorCategoria {
		public string Categora { get; set; }
		public decimal ValorMinimo { get; set; }
		public decimal ValorMaximo { get; set; }
		public decimal ValorTotal { get; set; }
	}
}
