﻿using System;
using System.Collections.Generic;

namespace Loja.Dominio.Entidades {
	public class Produto {
		private List<Produto> _produtos;
		public Produto() {
			_produtos = new List<Produto>();
		}

		public List<Produto> Listar() {
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Banana", Quantidade = 5, Valor = 16, DataValidade = DateTime.Now.AddDays(5), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Manga", Quantidade = 12, Valor = 9.4M, DataValidade = DateTime.Now.AddDays(8), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Rambutã", Quantidade = 35, Valor = 12, DataValidade = DateTime.Now.AddDays(30), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Abacaxi", Quantidade = 1, Valor = 3, DataValidade = DateTime.Now.AddDays(3), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Maracujá", Quantidade = 8, Valor = 7.5M, DataValidade = DateTime.Now.AddDays(15), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Cupuaçú", Quantidade = 3, Valor = 7, DataValidade = DateTime.Now.AddDays(365), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Acerola", Quantidade = 170, Valor = 8.3M, DataValidade = DateTime.Now.AddYears(2), Categoria = "Frutas" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Iphone X", Quantidade = 1, Valor = 8000, DataValidade = DateTime.Now.AddYears(2), Categoria = "Eletrônicos" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Galaxy S7 Edge", Quantidade = 1, Valor = 2000, DataValidade = DateTime.Now.AddYears(4), Categoria = "Eletrônicos" });
			_produtos.Add(new Produto() { Id = Guid.NewGuid(), Nome = "Pen Drive 32GB", Quantidade = 1, Valor = 150, DataValidade = DateTime.Now.AddYears(10), Categoria = "Eletrônicos" });

			return _produtos;
		}

		public Guid Id { get; set; }
		public string Nome { get; set; }
		public int Quantidade { get; set; }
		public decimal Valor { get; set; }
		public DateTime DataValidade { get; set; }
		public string Categoria { get; set; }
	}
}
