﻿using System;
using System.Linq;

namespace Introducao {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine("== Expressões Lambda com Array de inteiros");
			int[] numerosDesordenados = { 6, 9, 3, 2, 10, 8, 7, 4, 5, 1 };
			Console.WriteLine("Números");
			foreach (var num in numerosDesordenados) {
				Console.Write($"{num} ");
			}

			Console.WriteLine();
			Console.WriteLine("Números ordenados > que 2");
			var resultadoD = from num in numerosDesordenados // usando select
											 where num > 2
											 //orderby num
											 select num;

			var resultadoO = numerosDesordenados.Where(n => n > 2).OrderBy(n => n);

			foreach (int numero in resultadoO) { // usando lambda
				Console.WriteLine(numero);
			}
			////////////////////
			Console.WriteLine();
			Console.WriteLine("== Expressões Lambda com Array de strings");
			string[] cores = { "Preto", "Branco", "Amarelo", "Verde", "Cinza", "Lilás", "Azul", "Vermelho" };
			Console.WriteLine("Cores");
			foreach (var cor in cores) {
				Console.Write($"{cor} ");
			}

			Console.WriteLine();
			Console.WriteLine("Cores contendo r e o, ou e, ordenadas");
			var resultadoC = cores.Where(c => c.Contains("r") && c.Contains("o") || c.Contains("e")).OrderBy(c => c);
			foreach (var cor in resultadoC) {
				Console.WriteLine(cor);
			}
		}
	}
}
